package main.java.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class FileUtil {

    public List<String> readData(String fileName){
        List<String> data = new ArrayList<>();
        try {
            //TODO change the path
            FileReader fr = new FileReader("C:\\Users\\JJX\\IdeaProjects\\tsp_pga_cg\\TSP_PGA_bs\\src\\main\\resources\\testDataCollection\\"+fileName+".dat");
            BufferedReader bf = new BufferedReader(fr);
            String str;
            // 按行读取字符串
            while ((str = bf.readLine()) != null) {
                data.add(str);
            }
            bf.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

}
