package main.java;

import main.java.executor.BaseExecutor;
import main.java.executor.CoarseGrainedExecutor;
import main.java.model.City;
import main.java.model.Population;

import java.util.List;
import java.util.Map;

public class Main {

    private static GraphBuilder graphBuilder = new GraphBuilder();

    private static BaseExecutor baseExcutor = new BaseExecutor();

    private static CoarseGrainedExecutor coarseGrainedExecutor = new CoarseGrainedExecutor();

    private static long startTime, duration;

    public static void main(String[] args) {

        // use small example data to test
        // testByExample();

        //use standard data to test
        testByResource();

        //use random data to test
        // testByRandom();

    }

    public static void printResults(Population population){
        // Print final results
        // startTime = System.nanoTime();
        System.out.println("Finished");
        System.out.println("Final distance: " + population.getFittest().getDistance());
        System.out.println("Solution:");
        System.out.println(population.getFittest());
        // duration = System.nanoTime() - startTime;
        // System.out.println("Time elapsed: "+ duration/1000);
        // System.out.println("\n");
    }

    private static void testByExample(){
        //Holds all nodes
        List<City> tourManager = graphBuilder.buildExampleCities();

        //base code with single threaded
        startTime = System.currentTimeMillis();
        printResults(baseExcutor.fire(tourManager));
        duration = System.currentTimeMillis() - startTime;
        System.out.println("Time elapsed: "+ duration);
        System.out.println("\n");
        //coarse grained version multi-threaded
        startTime = System.currentTimeMillis();
        printResults(coarseGrainedExecutor.fire(tourManager));
        duration = System.currentTimeMillis() - startTime;
        System.out.println("Time elapsed: "+ duration);
        System.out.println("\n");
    }

    private static void testByResource(){
        Map<String, List<City>> collection = graphBuilder.readFromDataCollection();
        for (Map.Entry entry : collection.entrySet()){
            startTime = System.currentTimeMillis();
            List<City> tourManager = (List<City>) entry.getValue();
            printResults(baseExcutor.fire(tourManager));
            duration = System.currentTimeMillis() - startTime;
            System.out.println("Time elapsed: "+ duration);
            System.out.println("\n");
            startTime = System.currentTimeMillis();
            printResults(coarseGrainedExecutor.fire(tourManager));
            duration = System.currentTimeMillis() - startTime;
            System.out.println("Time elapsed: "+ duration);
            System.out.println("\n");
        }
    }

    private static void testByRandom(){
        List<City> tourManager = graphBuilder.buildCitiesRandomly(50);
        printResults(baseExcutor.fire(tourManager));
        printResults(coarseGrainedExecutor.fire(tourManager));
    }
}
