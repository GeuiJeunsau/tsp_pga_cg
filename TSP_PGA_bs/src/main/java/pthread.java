package main.java;
import main.java.model.Population;

public class pthread implements Runnable {
    public Population pops;

    public pthread(Population pop) {
        pops=pop;
    }
    public Population getPops(){
        return pops;
    }
    public void run() {
        for (int j = 0; j < 100; j++) {
            pops = GA.evolvePopulation(pops);
        }
    }
}
