package main.java.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A serial of nodes
 * Persist the routes
 */
public class Tour {
    private List<City> cityList;
    private double fitness = 0;
    private int distance = 0;

    // Creates a random individual
    public void generateIndividual(List<City> nodes) {
        // Loop through all our destination cities and add them to our tour
        for (int cityIndex = 0; cityIndex < nodes.size(); cityIndex++) {
            cityList.add(cityIndex, nodes.get(cityIndex));
        }
        // Randomly reorder the tour
        Collections.shuffle(cityList);
    }

    public List<City> getCityList() {
        return cityList;
    }

    public Tour setCityList(List<City> cityList) {
        this.cityList = cityList;
        return this;
    }

    // Gets the tours fitness
    public double getFitness() {
        if (fitness == 0) {
            fitness = 1/(double)getDistance();
        }
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public int getDistance() {
        if (distance == 0) {
            int tourDistance = 0;
            // Loop through our tour's cities
            for (int cityIndex=0; cityIndex < cityList.size(); cityIndex++) {
                // Get city we're travelling from
                City fromCity = cityList.get(cityIndex);
                // City we're travelling to
                City destinationCity;
                // Check we're not on our tour's last city, if we are set our
                // tour's final destination city to our starting city
                if(cityIndex+1 < cityList.size()){
                    destinationCity = cityList.get(cityIndex+1);
                }
                else{
                    destinationCity = cityList.get(0);
                }
                // Get the distance between the two cities
                tourDistance += fromCity.distanceTo(destinationCity);
            }
            distance = tourDistance;
        }
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Tour{" +
                "cityList=" + cityList +
                ", fitness=" + fitness +
                ", distance=" + distance +
                '}';
    }
}
