package main.java;

import main.java.model.City;
import main.java.model.Population;
import main.java.model.Tour;

import java.util.*;

public class GA {

    private static final double mutationRate = 0.015;
    private static final int tournamentSize = 5;

    // Evolves a population over one generation
    public static Population evolvePopulation(Population population){
        if (Objects.isNull(population) || Objects.isNull(population.getTourList())){
            return new Population();
        }
        List<Tour> newTourList = new ArrayList<>(population.getTourList().size());
        Boolean elitism = true;

        // Keep our best individual if elitism is enabled
        int elitismOffset = 0;
        if (elitism){
            newTourList.add(population.getFittest());
            elitismOffset = 1;
        }

        // Crossover population
        // Loop over the new population's size and create individuals from
        // Current population
        for (int i = elitismOffset; i < population.getTourList().size(); i++){
            // Select parents
            Tour parent1 = tournamentSelection(population);
            Tour parent2 = tournamentSelection(population);
            // Crossover parents
            Tour child = crossover(parent1, parent2);
            // Add child to new population
            newTourList.add(child);
        }
        // Mutate the new population a bit to add some new genetic material
        for (int i = elitismOffset; i < population.getTourList().size(); i++) {
            mutate(newTourList.get(i));
        }

        return new Population().setTourList(newTourList);
    }

    // Selects candidate tour for crossover
    private static Tour tournamentSelection(Population population){
        if (Objects.isNull(population) || population.getTourList() == null){
            return null;
        }
        Integer populationSize = population.getTourList().size();

        // Create a tournament population
        List<Tour> tournament = new ArrayList<>(populationSize);

        // For each place in the tournament get a random candidate tour and
        // add it
        for (int i = 0; i < tournamentSize; i++){
            int randomId = (int) (Math.random() * populationSize);
            tournament.add(population.getTourList().get(randomId));
        }

        return new Population().setTourList(tournament).getFittest();
    }

    // Applies crossover to a set of parents and creates offspring
    private static Tour crossover(Tour parent1, Tour parent2){
        // Create new child tour
        List<City> childCities = new ArrayList<>(parent1.getCityList().size());
        Tour child = new Tour().setCityList(childCities);

        // Get start and end sub tour positions for parent1's tour
        int startPos = (int) (Math.random() * parent1.getCityList().size());
        int endPos = (int) (Math.random() * parent1.getCityList().size());

        // add the sub tour from parent1 to our child
        if (startPos < endPos){
            child.getCityList().addAll(parent1.getCityList().subList(startPos, endPos));
        }else {
            child.getCityList().addAll(parent1.getCityList().subList(endPos, startPos));
        }

        // Loop through parent2's city tour
        //TODO can be optimized if use parallelStream() api?
        parent2.getCityList().stream().forEach(city -> {
            if (!child.getCityList().contains(city)){
                child.getCityList().add(city);
            }
        });

        return child;
    }

    // Mutate a tour using swap mutation
    private static void mutate(Tour tour) {
        // Loop through tour cities
        List<City> tourCity = tour.getCityList();

        for(int tourPos1=0; tourPos1 < tour.getCityList().size(); tourPos1++){
            // Apply mutation rate
            if(Math.random() < mutationRate){
                // Get a second random position in the tour
                int tourPos2 = (int) (tour.getCityList().size() * Math.random());

                // Get the cities at target position in tour
                City city1 = tour.getCityList().get(tourPos1);
                City city2 = tour.getCityList().get(tourPos2);

                // Swap them around
                tour.getCityList().set(tourPos2, city1);
                tour.getCityList().set(tourPos1, city2);
            }
        }
    }

}
