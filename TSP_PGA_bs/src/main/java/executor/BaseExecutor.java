package main.java.executor;

import main.java.GA;
import main.java.model.City;
import main.java.model.Population;

import java.util.List;

public class BaseExecutor implements Executor {
    @Override
    public Population fire(List<City> cities) {
        System.out.println("Single threaded version...");

        Population population = new Population();
        population.init(50,cities);
        System.out.println("Initial distance: " + population.getFittest().getDistance());

        // Evolve population for 100 generations
        population = GA.evolvePopulation(population);
        for (int i = 0; i < 100; i++) {
            population = GA.evolvePopulation(population);
        }

        return population;
    }
}
