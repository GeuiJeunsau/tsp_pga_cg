package main.java.executor;

import main.java.model.City;
import main.java.model.Population;
import main.java.model.Tour;
import main.java.pthread;

import java.util.ArrayList;
import java.util.List;


public class CoarseGrainedExecutor implements Executor {
    @Override
    public Population fire(List<City> cities) {
        System.out.println("CoarseGrainedExecutor");
        // Coarse Grained solution

        Population pops[] = new Population[4];
        Population new_pops[] = new Population[4];

        pthread runnable[] = new pthread[4];
        Thread thread[] = new Thread[4];

        // Initialize population and create threads
        for (int i = 0; i < pops.length; i++) {
            pops[i] = new Population();
            pops[i].init(100 / pops.length, cities);
            runnable[i] = new pthread(pops[i]);
            thread[i] = new Thread(runnable[i]);
            thread[i].start();
        }

        // Wait for all threads to finish 
        try{
            for(int i=0;i < new_pops.length;i++){
                thread[i].join();
                new_pops[i] = runnable[i].getPops();
            }
        }catch(Exception ex){
            System.out.println("There is an Exception: "+ ex);
        }

        // Find the shortest path and return the population
        Population result = pops[0];
        double d_min = Double.POSITIVE_INFINITY;
        int min = (int) d_min;
        int t_num = 0;
        for (int i = 0; i < new_pops.length; i++){
            if (min > new_pops[i].getFittest().getDistance()){
                min = new_pops[i].getFittest().getDistance();
                result = new_pops[i];
                t_num = i;
            }
        }
        System.out.println("Thread: " + t_num);
        return result;
    }
}
