package main.java;

import main.java.model.City;
import main.java.util.FileUtil;

import java.io.File;
import java.math.BigDecimal;
import java.security.PrivilegedExceptionAction;
import java.util.*;
import java.util.stream.Collectors;

public class GraphBuilder {

    FileUtil fileUtil = new FileUtil();


    public List<City> buildCitiesRandomly(Integer size){
        List<City> cityList = new ArrayList<>(size);
        Random random = new Random();
        while (size > 0){
            City city = new City(random.nextInt(10000), random.nextInt(10000));
            cityList.add(city);
            size--;
        }
        return cityList;
    }

    public List<City> buildExampleCities(){
        List<City> cityList = new ArrayList<>();
        City city = new City(60, 200);
        cityList.add(city);
        City city2 = new City(180, 200);
        cityList.add(city2);
        City city3 = new City(80, 180);
        cityList.add(city3);
        City city4 = new City(140, 180);
        cityList.add(city4);
        City city5 = new City(20, 160);
        cityList.add(city5);
        City city6 = new City(100, 160);
        cityList.add(city6);
        City city7 = new City(200, 160);
        cityList.add(city7);
        City city8 = new City(140, 140);
        cityList.add(city8);
        City city9 = new City(40, 120);
        cityList.add(city9);
        City city10 = new City(100, 120);
        cityList.add(city10);
        City city11 = new City(180, 100);
        cityList.add(city11);
        City city12 = new City(60, 80);
        cityList.add(city12);
        City city13 = new City(120, 80);
        cityList.add(city13);
        City city14 = new City(180, 60);
        cityList.add(city14);
        City city15 = new City(20, 40);
        cityList.add(city15);
        City city16 = new City(100, 40);
        cityList.add(city16);
        City city17 = new City(200, 40);
        cityList.add(city17);
        City city18 = new City(20, 20);
        cityList.add(city18);
        City city19 = new City(60, 20);
        cityList.add(city19);
        City city20 = new City(160, 20);
        cityList.add(city20);
        return cityList;
    }

    public Map<String, List<City>> readFromDataCollection(){
        Map<String, List<City>> collection = new HashMap<>(100);
        //TODO can add more data into collection,
        //TODO resource http://www.verysource.com/item/tsp_rar-402212.html
        collection.put("bays", turncatCoordinatesToCities(fileUtil.readData("bays")));
        collection.put("a280", turncatCoordinatesToCities(fileUtil.readData("a280")));
        return collection;
    }

    private static List<City> turncatCoordinatesToCities(List<String> coordinates){
        List<City> cityList = coordinates.stream().map(coo->{
            String[] pair = coo.trim().split("\\s+");
            System.out.println(pair[0]+","+pair[1]);
            City city = new City(new BigDecimal(pair[0]).intValue(), new BigDecimal(pair[1]).intValue());
            return city;
        }).collect(Collectors.toList());
        return cityList;
    }


}
