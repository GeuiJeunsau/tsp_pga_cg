package main.java;
import main.java.model.Population;
import main.java.model.Tour;

import java.util.ArrayList;
import java.util.List;

public class pthread implements Runnable {
    public Population population;
    public Population old_pop;

    pthread( Population old_p, Population new_p) {
        population = new_p;
        old_pop = old_p;
    }

    public void run() {
        int size =  old_pop.getTourList().size();
        List<Tour> newTourList = new ArrayList<>(size);
        for (int i = 0; i < size; i++){
            // Select parents
            Tour parent1 = GA.tournamentSelection(old_pop);
            Tour parent2 = GA.tournamentSelection(old_pop);
            // Crossover parents
            Tour child = GA.crossover(parent1, parent2);
            // Add child to new population
            newTourList.add(child);
        }
        // Mutate the new population a bit to add some new genetic material
        for (int i = 0; i < size; i++) {
            GA.mutate(newTourList.get(i));
        }
        population.setTourList(newTourList);
    }
}
