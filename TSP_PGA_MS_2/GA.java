package main.java;

import main.java.model.City;
import main.java.model.Population;
import main.java.model.Tour;

import java.util.*;

public class GA {

    private static final double mutationRate = 0.015;
    private static final int tournamentSize = 5;

    // Evolves a population over one generation
    public static Population evolvePopulation(Population population) {
        if (Objects.isNull(population) || Objects.isNull(population.getTourList())){
            return new Population();
        }

        List<Tour> newTourList = new ArrayList<>(population.getTourList().size());
        Boolean elitism = true;

        // Keep our best individual if elitism is enabled
        int elitismOffset = 0;
        if (elitism){
            newTourList.add(population.getFittest());
            elitismOffset = 1;
        }

        // Initialize population
        int thread_size = 4;
        Population[] old_p = new Population[thread_size];
        Population[] new_p = new Population[thread_size];
        int size = population.getTourList().size()/thread_size;

        // Split the population into sub-populations
        for(int j=0;j< old_p.length;j++){
            old_p[j] = new Population();
            old_p[j].setTourList(population.getTourList().subList(j*size,(j+1)*size));
        }

        // Create threads and start
        pthread[] runnable = new pthread[thread_size];
        Thread thread[] = new Thread[thread_size];
        for(int i = 0; i < runnable.length; i++) {
            new_p[i] = new Population();
            runnable[i] = new pthread(old_p[i], new_p[i]);
            thread[i] = new Thread(runnable[i]);
            thread[i].start();
        }

        // Wait for all threads to finish
        for(int i=0;i< thread.length;i++){
            try{
                thread[i].join();
            }
            catch(Exception ex){
                System.out.println("There is an Exception:" + ex);
            }
            new_p[i] = runnable[i].population;
        }

        // Combine the sub-populations into one
        Population result = new Population();
        List<Tour> result_list = new ArrayList<>(population.getTourList().size());
        for(int j=0;j<new_p.length;j++){
            result_list.addAll(new_p[j].getTourList());
        }
        result.setTourList(result_list);

        return result;
    }

    // Selects candidate tour for crossover
    public static Tour tournamentSelection(Population population){
        if (Objects.isNull(population) || population.getTourList() == null){
            return null;
        }
        Integer populationSize = population.getTourList().size();

        // Create a tournament population
        List<Tour> tournament = new ArrayList<>(populationSize);

        // For each place in the tournament get a random candidate tour and
        // add it
        for (int i = 0; i < tournamentSize; i++){
            int randomId = (int) (Math.random() * populationSize);
            tournament.add(population.getTourList().get(randomId));
        }

        return new Population().setTourList(tournament).getFittest();
    }

    // Applies crossover to a set of parents and creates offspring
    public static Tour crossover(Tour parent1, Tour parent2){
        // Create new child tour
        List<City> childCities = new ArrayList<>(parent1.getCityList().size());
        Tour child = new Tour().setCityList(childCities);

        // Get start and end sub tour positions for parent1's tour
        int startPos = (int) (Math.random() * parent1.getCityList().size());
        int endPos = (int) (Math.random() * parent1.getCityList().size());

        // add the sub tour from parent1 to our child
        if (startPos < endPos){
            child.getCityList().addAll(parent1.getCityList().subList(startPos, endPos));
        }else {
            child.getCityList().addAll(parent1.getCityList().subList(endPos, startPos));
        }

        // Loop through parent2's city tour
        parent2.getCityList().stream().forEach(city -> {
            if (!child.getCityList().contains(city)){
                child.getCityList().add(city);
            }
        });

        return child;
    }

    // Mutate a tour using swap mutation
    public static void mutate(Tour tour) {
        // Loop through tour cities
        List<City> tourCity = tour.getCityList();

        for(int tourPos1=0; tourPos1 < tour.getCityList().size(); tourPos1++){
            // Apply mutation rate
            if(Math.random() < mutationRate){
                // Get a second random position in the tour
                int tourPos2 = (int) (tour.getCityList().size() * Math.random());

                // Get the cities at target position in tour
                City city1 = tour.getCityList().get(tourPos1);
                City city2 = tour.getCityList().get(tourPos2);

                // Swap them around
                tour.getCityList().set(tourPos2, city1);
                tour.getCityList().set(tourPos1, city2);
            }
        }
    }
}
