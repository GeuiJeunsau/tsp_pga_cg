package main.java;

import main.java.executor.MasterSlaveExecutor;
import main.java.model.City;
import main.java.model.Population;

import java.util.List;
import java.util.Map;

public class Main {

    private static GraphBuilder graphBuilder = new GraphBuilder();

    private static MasterSlaveExecutor masterSlaveExecutor = new MasterSlaveExecutor();

    public static void main(String[] args) {

        //Holds all nodes
        List<City> tourManager = graphBuilder.buildExampleCities();

        //master slave version multi-threaded
        printResults(masterSlaveExecutor.fire(tourManager));


        //use standard data to test
      //   testByResource();

        //use random data to test
       // testByRandom();

    }

    public static void printResults(Population population){
        // Print final results
        System.out.println("Finished");
        System.out.println("Final distance: " + population.getFittest().getDistance());
        System.out.println("Solution:");
        System.out.println(population.getFittest());
    }

    private static void testByResource(){
        Map<String, List<City>> collection = graphBuilder.readFromDataCollection();
        for (Map.Entry entry : collection.entrySet()){
            List<City> tourManager = (List<City>) entry.getValue();
            //TODO change parallelism
            printResults(masterSlaveExecutor.fire(tourManager));
        }
    }

    private static void testByRandom(){
        List<City> tourManager = graphBuilder.buildCitiesRandomly(50);
        //TODO change parallelism
        printResults(masterSlaveExecutor.fire(tourManager));
    }



}
