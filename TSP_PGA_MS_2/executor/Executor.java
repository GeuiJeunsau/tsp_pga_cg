package main.java.executor;

import main.java.model.City;
import main.java.model.Population;

import java.util.List;

public interface Executor {

    Population fire(List<City> cities) throws InterruptedException;
}
