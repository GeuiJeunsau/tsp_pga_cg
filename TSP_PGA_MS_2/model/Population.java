package main.java.model;

import java.util.*;

/**
 * A set of Tour
 * persist all the candidate tours
 */
public class Population {

    private List<Tour> tourList;

    public void init(Integer size, List<City> nodes){
        this.tourList = new ArrayList<>(size);
        while (size > 0){
            List<City> cityList = new ArrayList<>(nodes.size());
            Tour newTour = new Tour().setCityList(cityList);
            newTour.generateIndividual(nodes);
            tourList.add(newTour);
            size--;
        }
    }

    public Tour getFittest() {
        if (Objects.nonNull(tourList)){
            //TODO maybe can use parallelStream() api
            Optional<Tour> fittest = tourList.stream().max(Comparator.comparing(Tour::getFitness));
            if (fittest.isPresent()){
                return fittest.get();
            }
        }
        return null;
    }

    public List<Tour> getTourList() {
        return tourList;
    }

    public Population setTourList(List<Tour> tourList) {
        this.tourList = tourList;
        return this;
    }

}
